# [Unreleased]

# [25.1.0]

- Fix 3 functions that did not properly encode the parameter data
- Add missing messages related to piezo controllers, laser control, quad, etc

# [25.0.1]

- Switch to flit for packaging

# [25.0.0]

Initial release

Please note that not _all_ of the messages in the protocol are represented at this time
In particular, those related to Piezo controllers, Laser Control, and Quad control are not yet implemented


[Unreleased]: https://gitlab.com/yaq/thorlabs-apt-protocol/-/compare/v25.1.0...master
[25.1.0]: https://gitlab.com/yaq/thorlabs-apt-protocol/-/compare/v25.0.1...v25.1.0
[25.0.1]: https://gitlab.com/yaq/thorlabs-apt-protocol/-/compare/v25.0.0...v25.0.1
[25.0.0]: https://gitlab.com/yaq/thorlabs-apt-protocol/-/tags/v25.0.0
